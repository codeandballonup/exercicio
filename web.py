import requests

def main():
    print('''##############################''')
    print('### Consulta de CEP ###')
    print('''##############################''')
    print('')

    cpf = input('Digite o CEP para a consulta\n')

    if len(cpf) != 8:
        print('Quantidade de D�gito invalido!')
        exit()

    request = requests.get('https://viacep.com.br/ws/{}/json/'.format(cpf))

    add_data = request.json()

    if 'erro' not in add_data:
        print('==> CEP Encontrado <==')

        print('CEP: {}'.format(add_data['cep']))
        print('Logradouro: {}'.format(add_data['logradouro']))
        print('Complemento: {}'.format(add_data['complemento']))
        print('Bairro: {}'.format(add_data['bairro']))
        print('Cidade: {}'.format(add_data['localidade']))
        print('Estado: {}'.format(add_data['uf']))
    else:
        print('{}: CEP Invalido'.format(cpf))
    
    print('-------------------------------------')
    
    option = int(input('Deseja realizar uma nova consulta ?\n1. Sim\n2. Sair\n'))
    if option == 1:
        main()
    else:
        print('Saindo....')

if __name__ == '__main__':
    main()